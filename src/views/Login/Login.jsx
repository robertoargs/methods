import React, {Component} from 'react';
import { setLogin } from "../../api/api"; //It allows the user to login in the page
import session from "../../models/session"; //It allows to know if the user has accesced before
import { setData } from "../../models/token";//It allows to get token and set in in local storage
import '../../style/Login.css'; //CSS for the login page (works for this component)

export default class LoginAuth extends Component{ 
  //Component State
  state = {
    email: "",
    password: "",
  };

  componentDidMount() {
    const mySession = session();
    if (mySession === true) {
      window.location.href = "/admin"
    }
  }

  handleChange(e, input) {
    const value = e.target.value;
    this.setState({
    //Catch the param in state
      [input]:value
    })
  }

  sendData = async() => {
    const {email, password} = this.state;

    if(email === "" || email === null) {
      alert("Ingrese su Correo Electrónico")
    } 
    else if(password === "" || password === null) {
      alert("Ingrese su Contraseña")
    }
    else {   
      let jsn = {
        "email":email,
        "password":password
      }
      //console.log(jsn)

      //response
      const res = await setLogin(jsn);
      //response info
      const success = res.success;
      const token = res.token;
      const message = res.messages;
      
      if(success === true) {
        alert(message + "Bienvenido")
        setData("token", token);
        window.location.href="/admin";
        console.log(res);
      }
      else {
        console.log(res);
      }
      
    }
  }

  setRegister = () => {
    window.location.href="/register";
  }


  render() {
    //<Modal/>
    return (
      <>
      
      <div className="sidenav">
         <div className="login-main-text">
            <h2>Application</h2>
            <h2>Login Page</h2>
            <p>Login or register from here to access.</p>
         </div>
      </div> 
      
      <div className="main">
         <div className="col-md-6 col-sm-12">
            <div className="login-form">
               <div>
                  <div className="form-group">
                     <label>User Name</label>
                     <input type="text" className="form-control" placeholder="User Name" onChange={(event) => this.handleChange(event,"email")}/>
                  </div>

                  <div className="form-group">
                     <label>Password</label>
                     <input type="password" className="form-control" placeholder="Password" onChange={(event) => this.handleChange(event,"password")}/>
                  </div>

                  <button type="submit" className="btn btn-black" onClick={this.sendData}>Login</button>
                  <button type="submit" className="btn btn-secondary" onClick={this.setRegister}>Register</button>

               </div>
            </div>
         </div> 
      </div> 
      </>
    );
  }
}     