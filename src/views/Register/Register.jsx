import React, {Component} from 'react';
import { setRegister } from "../../api/api"; //It allows the user to login in the page
import session from "../../models/session"; //It allows to know if the user has accesced before
//import { setData } from "../../models/token";//It allows to get token and set in in local storage
import '../../style/Register.css'; //CSS for the login page (works for this component)

export default class LoginAuth extends Component{ 
  //Component State
  state = {
    name:"",
    lastName:"",
    email: "",
    password: "",
    confirm:""
  };

  componentDidMount() {
    const mySession = session();
    if (mySession === true) {
      window.location.href = "/home"
    }
  }

  handleChange(e, input) {
    const value = e.target.value;
    this.setState({
    //Catch the param in state
      [input]:value
    })
  }

  submitData = async() => {
    const {name, lastName, email, password, confirm} = this.state;

    if(name === "" || name === null) {
      alert("Ingrese su Nombre")
    } 
    else if(lastName === "" || lastName === null) {
      alert("Ingrese su Apellido")
    }
    else if(email === "" || email === null) {
      alert("Ingrese su Correo Electrónico")
    }
    else if(password === "" || password === null) {
      alert("Ingrese su Contraseña")
    }
    else if(confirm === "" || confirm === null) {
      alert("Ingrese la Confirmación de su Contraseña")
    }
    else {   
      if (password === confirm) {
        let jsn = {
          "name":name,
          "lastName":lastName,
          "uMail":email,
          "uPass":password,
          "confirm":confirm
        }
      console.log(jsn)

      //response
      const res = await setRegister(jsn);
      //response info
      const success = res.success;
      const message = res.messages;
      
      if(success === true) {
        alert(message);
        window.location.href="/login";
        console.log(res);
      }
      else {
        alert(message);
        console.log(res);
      }

      }
      else {
        alert("Las contraseñas no coinciden")
      }
    }
  }

  render() {
    return (
      <>
      <div className="sidenav">
         <div className="login-main-text">
            <h2>Numerical Methods</h2>
            <h2>Register Page</h2>
            <p>Register yourself here!</p>
         </div>
      </div> 
      
      <div className="main">
         <div className="col-md-6 col-sm-12">
            <div className="login-form">
               <div>
                  <div className="form-group">
                     <label>Name: </label>
                     <input id="nameInput" type="text" className="form-control" placeholder="Name" onChange={(event) => this.handleChange(event,"name")}/>
                  </div>

                  <div className="form-group">
                     <label>Last Name: </label>
                     <input type="text" className="form-control" placeholder="Last Name" onChange={(event) => this.handleChange(event,"lastName")}/>
                  </div>

                  <div className="form-group">
                     <label>Email: </label>
                     <input type="text" className="form-control" placeholder="Email" onChange={(event) => this.handleChange(event,"email")}/>
                  </div>

                  <div className="form-group">
                     <label> Password: </label>
                     <input type="password" className="form-control" placeholder="Password" onChange={(event) => this.handleChange(event,"password")}/>
                  </div>

                  <div className="form-group">
                     <label> Confirm your password: </label>
                     <input type="password" className="form-control" placeholder="Confirm your Password" onChange={(event) => this.handleChange(event,"confirm")}/>
                  </div>
 
                  <button type="submit" className="btn btn-black" onClick={this.submitData}>Submit</button>

               </div>
            </div>
         </div> 
      </div> 
      </>
    );
  }
}     