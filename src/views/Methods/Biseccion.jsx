import React, { Component } from 'react';
import { Table } from 'reactstrap';
import {methodBiseccion} from '../../api/api';

export default class Prueba extends Component {

  state = {
    funcion:"",
    xi:"",
    xf:"",
    errorPerm:"",
    arrData: []
  }

  handleChange(e, input) {
    const value = e.target.value;
    console.log(this.state)
    this.setState({
      [input]:value
    })
  }

  calcular = async() => {
    const {xi, xf, funcion, errorPerm, arrData} = this.state;
    //(667.38/x) * (1-e^((-10/68.1)*x)) - 40 --> función de prueba
    let jsn = {
      "funcion":funcion,
      "xi": xi,
      "xf": xf,
      "errorPerm": errorPerm
    }
    console.log(jsn);
    //Result contains and object with MathResult and bodyTable
    const result = await methodBiseccion(jsn);
    const MathResult = result.MathResult;
    const bodyTable = result.bodyTable;
    const message = result.message;
    
    this.setState({
      arrData: MathResult
    })


    //NOOOOOOOOOOOOOOOOOOOOOOOOOOO
    //document.getElementById("bodyTable").innerHTML = "";
    //document.getElementById("bodyTable").innerHTML = result.bodyTable;
  }

  render() { 
    return ( 
      <>
        <div className="content">
          <label>Función:</label> 
          <input 
            id="funcion" 
            type="text"
            onChange={(event) => this.handleChange(event,"funcion")} />

          <label>Xi:</label>
          <input id="xi" type="text" onChange={(event) => this.handleChange(event,"xi")} />

          <label>Xf:</label>
          <input id="xf" type="text" onChange={(event) => this.handleChange(event,"xf")} />

          <label>Error perm:</label>
          <input id="errorPerm" type="text" onChange={(event) => this.handleChange(event,"errorPerm")} />

          <button className="btn btn-danger" onClick={this.calcular}>Calcular</button>
        

          <Table>
            <thead>
              <tr>
                <th>Iteración</th>
                <th>Xi</th>
                <th>Xf</th>
                <th>Xm</th>
                <th>Ea (%)</th>
              </tr>
            </thead>

            <tbody>
              {
                this.state.arrData.map( (d,i) => (
                  <tr key={i}>

                    <td>
                      {d.iter}
                    </td>
                  
                    <td>
                      {d.Xi}
                    </td>

                    <td>
                      {d.Xf}
                    </td>

                    <td>
                      {d.Xm}
                    </td>

                    <td>
                      {d.Ea}
                    </td>

                  </tr>
                ))
              }
            </tbody>
          </Table>
        </div>
      </>
    );
  }
}
