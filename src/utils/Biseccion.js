import { parse, chain } from 'mathjs';

export function biseccion(funcion, xi, xf, errorPerm) {
  let eqn = parse(funcion);
  let fxi = eqn.evaluate({ x: xi });
  let fxf = eqn.evaluate({ x: xf });
  let iter = 1;
  let Xmedio;
  let fxm;
  let Ea;
  let array = [];
  let isThereRoot = fxi * fxf;

  if (isThereRoot > 0) {
    alert("No hay solución en el intérvalo indicado");
  } else if (isThereRoot === 0) {
    if (fxi === 0) {
      alert("El extremo izquierdo es una raíz");
    } else {
      alert("El extremo derecho es una raíz");
    }
  } else {
    alert("Existe una raíz en el intérvalo indicado");
    let numerador = chain(xf).subtract(xi).done();
    let denominador = chain(xf).add(xi).done();
    Ea = chain(numerador).multiply(1/denominador).multiply(100).done();
    
    while (errorPerm < Ea) {
      Xmedio = chain(xi).add(xf).multiply(0.5).done();
      numerador = chain(xf).subtract(xi).done();
      denominador = chain(xf).add(xi).done();
      Ea = chain(numerador).multiply(1/denominador).multiply(100).done();
      let jsn = {
        "iter": iter,
        "Xi":xi,
        "Xf":xf,
        "Xm":Xmedio,
        "Ea":Ea
      };

      array.push(jsn);

      fxm = eqn.evaluate({ x: Xmedio });

      let condition = fxi*fxm;

      if (condition < 0) {
        xf = Xmedio;
      } else if (condition > 0) {
        xi = Xmedio;
      } else {
        const newLocal = "paro";
        console.log(newLocal)
        break
      }

      let increment = chain(iter).add(1).done();
      iter = increment;
      
    } // end while loop
  

  } // end if statement

  let body="";
    for (let i in array) {
      body += 
      `
        <tr>
          <td>
            ${array[i].iter}
          </td>
          <td>
            ${array[i].Xi}
          </td>
          <td>
            ${array[i].Xf}
          </td>
          <td>
            ${array[i].Xm}
          </td>
          <td>
            ${array[i].Ea}
          </td>
        </tr>
      `
    }


  let jsnResult = {
    "MathResult":array,
    "bodyTable":body
  }

  return jsnResult; // result
} // end function
