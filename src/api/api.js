import axios from "axios";
import * as CONS from "./constants";
//import {getData} from '../models/token';

//const token = getData("token");

//INICIO DE SESION
export async function setLogin(userData) {
  try {
    const res = await axios.post(CONS.login, userData);
    return res.data;
  }
  catch(err) {
    return err;
  }
}

//REGISTRO DE USUARIO
export async function setRegister(userData) {
  try {
    const res = await axios.post(CONS.register,userData);
    return res.data;
  }
  catch(err) {
    return err;
  }
}

//METODO DE BISECCION
export async function methodBiseccion(functionData) {

  try {
    const res = await axios.post(CONS.biseccion, functionData);
    return res.data;
  }
  catch(err) {
    return err;
  }
}
