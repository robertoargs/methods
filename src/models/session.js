import { getData } from './token';

const token = getData("token");

export default function session() {
  if (token === undefined || token === "" || token === null) {
    return false;
  } else {
    return true;
  }
}