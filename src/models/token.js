export function getData (index) {
  return window.localStorage.getItem(index);
}

export function setData(index,value) {
  window.localStorage.setItem(index,value);
}

export function deleteData(index){
  window.localStorage.removeItem(index);	
}
