import React from "react";
import ReactDOM from "react-dom";
//import { createBrowserHistory } from "history";
//import { Router, Route, Switch, Redirect } from "react-router-dom";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.css";
import "assets/scss/paper-dashboard.scss?v=1.2.0";
import "assets/demo/demo.css";
import "perfect-scrollbar/css/perfect-scrollbar.css";

import AdminLayout from "layouts/Admin.js";
//import Login from 'layouts/Login';
import Login from './views/Login/Login';
import Register from './views/Register/Register';
import 'semantic-ui-css/semantic.min.css'

//import LoginAuth from "./views/Login/Login";

//const hist = createBrowserHistory();

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route path="/login" render={(props) => <Login {...props} />} />
      <Route path="/register" render={(props) => <Register {...props} />} />
      <Route path="/admin" render={(props) => <AdminLayout {...props} />} />
      <Redirect from="*" to="/login" />
    </Switch>
  </BrowserRouter>,
  document.getElementById("root")
);
